import sublime, sublime_plugin

class ToggleWhitesmithsCommand(sublime_plugin.ApplicationCommand, sublime_plugin.EventListener):
	def __init__(self):
		self.current_setting = False
		sublime_plugin.ApplicationCommand.__init__(self)
		sublime_plugin.EventListener.__init__(self)
	def run(self, args={}):
		for view in sublime.active_window().views():
			current_setting = view.settings().get("whitesmiths_enabled", False)
			view.settings().set("whitesmiths_enabled", not current_setting)
			self.current_setting = view.settings().get("whitesmiths_enabled", False)

	def on_new(self, view):
		view.settings().set("whitesmiths_enabled", self.current_setting)

	def on_clone(self, view):
		self.on_new(view)

	def on_load(self, view):
		self.on_new(view)

	def on_activated(self, view):
		if(view.settings().get("whitesmiths_enabled", False)):
			self.current_setting = True

	def is_checked(self):
		return self.current_setting